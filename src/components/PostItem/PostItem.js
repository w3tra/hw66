import React from 'react';
import './PostItem.css';
import {NavLink} from "react-router-dom";

const PostItem = (props) => {
  return(
    <div className="post-item">
      <div className="post-time">Created on: {props.time}</div>
      <p>{props.title}</p>
      <NavLink to={"/posts/" + props.id} className="post-btn">Read more</NavLink>
      <button className="post-btn" onClick={props.error}>DONT CLICK</button>
    </div>
  )
};

export default PostItem;