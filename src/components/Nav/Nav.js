import React from 'react';
import './Nav.css';
import {NavLink} from "react-router-dom";

const Nav = () => {
  return(
    <nav className='main-nav'>
      <ul>
        <li><NavLink to="/">Posts</NavLink></li>
        <li><NavLink to="/posts/add">Add</NavLink></li>
      </ul>
    </nav>
  )
};

export default Nav;