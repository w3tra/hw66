import React, { Component } from 'react';
import axios from 'axios';
import './Posts.css';
import withErrorHandler from "../../hoc/withErrorHandler/withErrorHandler";
import PostItem from "../../components/PostItem/PostItem";
import ErrorBoundary from "../../components/ErrorBoundary/ErrorBoundary";

class Posts extends Component {

  state = {
    posts: []
  };

  componentDidMount() {
    axios.interceptors.request.use((req) => {
      console.log('[request interceptor]', req);
      return req;
    });
    axios.get('/post.json').then(response => {
      response.data ? this.setState({posts: response.data}) : this.setState({posts: []});
    });
    axios.interceptors.response.use(res => {
      console.log('[response interceptor]', res);
      return res;
    })
  }

  makeError = () => {throw new Error('I crashed!')};

  render() {
    return(
      <div className='container'>
        <ErrorBoundary>
          {Object.keys(this.state.posts).map(itemId => {
            return (
              <PostItem  key={itemId}
                         id={itemId}
                         time={this.state.posts[itemId].time}
                         title={this.state.posts[itemId].title}
                         error={this.makeError} />
            )
          })}
        </ErrorBoundary>
      </div>
    )
  }
}

export default withErrorHandler(Posts, axios);